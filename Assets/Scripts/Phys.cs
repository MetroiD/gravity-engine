using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Phys : MonoBehaviour
{
    public static event System.Action Event;
    private List<CirclePhys> PlanScripts = new List<CirclePhys>();
    public FpsText FpsReader;
    private Vector3 StartVec;

    private IEnumerator DelayAction()
    {
        yield return new WaitForSeconds(1);
        Event += Tick;
    }

    void Start()
    {
        StartCoroutine(DelayAction());
        SetTimescale(1f);
        //Event += Tick;
    }

    public void AddPlanetary(CirclePhys Plan)
    {
        if(!PlanScripts.Contains(Plan))
        {
            PlanScripts.Add(Plan);
        }
    }
    public void RemovePlanetary(CirclePhys Plan)
    {
        PlanScripts.Remove(Plan);
    }
    private void ClonePlanetary(GameObject PlanetaryOrig, Vector3 WorldPos)
    {
        GameObject Clone = Instantiate(PlanetaryOrig, WorldPos, Quaternion.identity);
        AddPlanetary(Clone.GetComponent<CirclePhys>());
    }

    private void Tick()
    {
        for (int i = 0; i < PlanScripts.Count; i++)
        {
            Vector3 G1pos = PlanScripts[i].Position;

            for (int ii = 0; ii < PlanScripts.Count; ii++)
            {
                if (ii != i)
                {
                    //A1=Dir*M2/(R^2)
                    Vector3 G2pos = PlanScripts[ii].Position;

                    float G2Mass = PlanScripts[ii].Mass;
                    float G1G2Dist = Vector3.Distance(G2pos, G1pos)/4;

                    float force = (G2Mass / Mathf.Pow(G1G2Dist, 2));
                    Vector3 Direction = (G2pos - G1pos).normalized;

                    PlanScripts[i].Accel += Direction * force;

                }
            }
        //PlanScripts[i].Accel /= 10000;
        PlanScripts[i].Move(Time.deltaTime * 100);
        }


    }

    private void FixedUpdate()
    {
        if (Event != null) Event();
        FpsReader.PPS = (int)(1f / Time.unscaledDeltaTime);
    }

    void Update()
    {
        //if (Input.GetButtonDown("Fire1")) Fire1Down();
        //if (Input.GetButtonUp("Fire1")) Fire1Up();
    }

    public void SetTimescale(float TimeScale)
    {
        Time.timeScale = Mathf.Clamp(TimeScale, 0.1f, 100f);
    }


    public void Fire1Down(PointerEventData Mouse)
    {
        Vector2 screenPosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        Vector2 worldPosition = Camera.main.ScreenToWorldPoint(screenPosition);
        StartVec = worldPosition;
    }
    public void Fire1Up(PointerEventData Mouse)
    {
        Vector2 screenPosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        Vector2 worldPosition = Camera.main.ScreenToWorldPoint(screenPosition);
        ClonePlanetary(PlanScripts[0].gameObject, StartVec);
        PlanScripts[PlanScripts.Count - 1].Mass = 100f;
        PlanScripts[PlanScripts.Count - 1].Velocity = ((Vector3)worldPosition - StartVec) / 50;
    }
    


}
