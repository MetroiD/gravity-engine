using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CameraController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    private Camera Cam;
    private Transform CamTrans;
    private Transform Lock;
    private BoxCollider2D Collider;
    private float ZoomFactor;

    [SerializeField]
    private GameObject Options;

    // Start is called before the first frame update
    void Start()
    {

        //SetVSYNC
        QualitySettings.vSyncCount = 1;

        //SetUPS (60 UPS = 0.0166; 75 UPS = 0.0133; 1/TargetFPS)
        Time.fixedDeltaTime = 0.0133f;  

        //Application.targetFrameRate = 60;
        //Time.captureFramerate = 60;

        CamTrans = GetComponent<Transform>();
        Cam = GetComponent<Camera>();
        Collider = GetComponent<BoxCollider2D>();
        ZoomFactor = 250;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Options.SetActive(!Options.activeSelf);
        }

        if (Input.GetButtonDown("Fire2"))
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);

            RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);


            if (hit.collider != null)
            { Lock = hit.collider.transform; }
            else { Lock = null; }
        }
        
        if (Lock != null)
        {
            Vector3 v = new Vector3(Lock.position.x, Lock.position.y, -10f);
            CamTrans.position = v;
        }

        int w = System.Convert.ToInt32(Input.GetKey("w"));
        int a = System.Convert.ToInt32(Input.GetKey("a"));
        int s = System.Convert.ToInt32(Input.GetKey("s"));
        int d = System.Convert.ToInt32(Input.GetKey("d"));
        
        ZoomFactor -= Input.mouseScrollDelta.y * 30; 
        if (ZoomFactor < 100)  { ZoomFactor = 70;  }
        if (ZoomFactor > 1000) { ZoomFactor = 1000; }

        if(Lock == null) CamTrans.position += new Vector3((d-a) * ZoomFactor * Time.deltaTime * 2, (w-s) * ZoomFactor * Time.deltaTime * 2, 0f);
        Cam.orthographicSize = ZoomFactor;
        Collider.size = (new Vector2(Cam.pixelWidth, Cam.pixelHeight)/310)*ZoomFactor;
    }
    void OnRectTransformDimensionsChange()
    {
        
        //print("asdasd");
        //
    }

    public void OnPointerDown(PointerEventData D)
    {
        //print("asdasdasd "+$"{D}");
        print(D.position);

    }
    public void OnPointerUp(PointerEventData D)
    {
        //print("asdasdasd2 " + $"{D}");
    }

    private void HideIfClickedOutside(GameObject panel)
    {
        if (Input.GetMouseButton(0) && panel.activeSelf && !RectTransformUtility.RectangleContainsScreenPoint(panel.GetComponent<RectTransform>(), Input.mousePosition, Camera.main))
        {
            panel.SetActive(false);
        }
    }


}
