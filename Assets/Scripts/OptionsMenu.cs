using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsMenu : MonoBehaviour
{
    [SerializeField]
    private GameObject FPSToggle;
    [SerializeField]
    private GameObject FPSDisplay;


    void Start()
    {
        print("START");
        FPSToggle.GetComponent<Toggle>().isOn = FPSDisplay.activeSelf;        
    }
    public void ExitButton()
    {
        Application.Quit();
    }
    public void ToggleFPS()
    {
        FPSDisplay.SetActive(FPSToggle.GetComponent<Toggle>().isOn);
    }

}
