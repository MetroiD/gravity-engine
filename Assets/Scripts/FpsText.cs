using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FpsText : MonoBehaviour
{
    private int FPS = 0;
    public int PPS = 0;
    public Text FpsTextObj;

    void Update()
    {
        FPS = (int)(1f / Time.unscaledDeltaTime);
        //UPS = (int)(1f / Time.deltaTime);
        FpsTextObj.text = $"FPS: {FPS}\nPPS: {PPS}";
    }
}
