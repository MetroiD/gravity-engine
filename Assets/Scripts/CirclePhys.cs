using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CirclePhys : MonoBehaviour
{

    private Transform CircleTransform;
    private SpriteRenderer SpriteRend;
    private TrailRenderer TrailRend;
    private Phys PhysController;

    [SerializeField]
    public Vector3 Position = new Vector3();
    [SerializeField]
    public Vector3 Velocity = new Vector3();
    [SerializeField]
    public Vector3 Accel = new Vector3();
    [SerializeField]
    public float Mass = 0f;
    [SerializeField]
    public float Apply = 0f;

    void Start()
    {
        PhysController = Object.FindObjectOfType<Phys>();
        PhysController.AddPlanetary(this);
        
        CircleTransform = GetComponent<Transform>();
        SpriteRend = GetComponent<SpriteRenderer>();
        TrailRend = GetComponent<TrailRenderer>();
        Position = CircleTransform.position;
        if (Mass == 0) Mass = 0.1f;
        if (Apply == 0) Apply = 1f; 
        SetColor( Color.HSVToRGB(Random.Range(0f, 1f),1f,1f), new Vector2(3f,0), new Vector3(10f,10f,1f) );
        Velocity *= 4f*2f ;
    }

    void SetColor(Color Col, Vector2 TrailWidth, Vector3 Size)
    {
        CircleTransform.localScale = Size;
        SpriteRend.color = Col;
        TrailRend.startColor = Col;
        TrailRend.endColor = Col;
        TrailRend.startWidth = TrailWidth.x;
        TrailRend.endWidth = TrailWidth.y;
    }
    void OnDestroy()
    {
        PhysController.RemovePlanetary(this);
    }

    public void Move(float TimeCoeff)
    {
        Velocity += Accel * TimeCoeff * Apply;
        Accel = new Vector3(0, 0, 0);
        Position += Velocity * TimeCoeff;
    }

    void Update()
    {
        CircleTransform.position = Position;
    }
}
